public class AppMoveable {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Moveable point1 = new MovablePoint(10, 20);
        Moveable point2 = new MovablePoint(40, 30);
        System.out.println("POINT 1 :--------------");
        System.out.println(point1.toString());
        point1.moveUp();
        point1.moveRight();
        System.out.println(point1.toString());

        System.out.println("POINT 2 :--------------");
        System.out.println(point2.toString());
        point2.moveDown();
        point2.moveLeft();
        System.out.println(point2.toString());
    }
}
